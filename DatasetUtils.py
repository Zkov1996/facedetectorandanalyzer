import os
import pickle
from typing import Dict, List

from numpy import ndarray

from FaceDetector import FaceDetector


class DatasetUtils:

    @staticmethod
    def FacesToDataset(path: str) -> Dict[str, List[List[ndarray]]]:
        datasets: Dict[str, List[List[ndarray]]] = {}

        for faceFolder in os.listdir(path):
            name = faceFolder.split('/')[-1]
            datasets[name] = DatasetUtils.FaceToDataset(os.path.join(path, faceFolder))

        return datasets

    @staticmethod
    def FaceToDataset(face_folder: str) -> List[List[ndarray]]:

        dataset: List[List[ndarray]] = []

        for image in os.listdir(face_folder):
            dataset.append(FaceDetector.FaceImageToEncodings(os.path.join(face_folder, image), "cnn"))
            # dataset.append(FaceDetector.FaceImageToEncodings(os.path.join(face_folder, image)))

        return dataset

    @staticmethod
    def SaveDatasets(path: str, datasets: Dict[str, List[List[ndarray]]]) -> None:
        f = open(path, "wb")
        f.write(pickle.dumps(datasets))
        f.close()

    @staticmethod
    def LoadDatasets(path: str) -> Dict[str, List[List[ndarray]]]:
        f = open(path, "rb")
        datasets = pickle.loads(f.read())
        f.close()
        return datasets
