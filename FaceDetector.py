from typing import List, Tuple

import face_recognition
import imutils
from cv2 import cv2
from numpy import ndarray


class FaceDetector:

    @staticmethod
    def FaceImageToEncodings(image_path: str, model: str = "hog") -> List[ndarray]:
        image: ndarray = cv2.imread(image_path)
        rgb: ndarray = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        rgbScaled = imutils.resize(rgb, width=750)

        # detect the (x, y)-coordinates of the bounding boxes
        # corresponding to each face in the input image
        boxes: List[Tuple[int, int, int, int]] = face_recognition.face_locations(rgbScaled, model=model)

        # compute the facial embedding for the face
        detected_faces: List[ndarray] = face_recognition.face_encodings(rgbScaled, boxes)

        return detected_faces

    @staticmethod
    def CompareFaces(datasets, faces: list):
        for face in faces:
            face_recognition.compare_faces(datasets, face)
        pass
