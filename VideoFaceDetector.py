import face_recognition
import imutils
from cv2 import cv2

from DatasetUtils import DatasetUtils


class VideoFaceDetector:

    @staticmethod
    def DetectInVideo():

        cv2.namedWindow("frame")

        datasets: dict = DatasetUtils.LoadDatasets("dataset.bin")
        datasetarr = [
            (name_dataset_index * len(dataset) + face_dataset_index * len(face_dataset) + face_index, face_name, face)
            for (name_dataset_index, (face_name, dataset)) in enumerate(datasets.items())
            for (face_dataset_index, face_dataset) in enumerate(dataset)
            for (face_index, face) in enumerate(face_dataset)
        ]

        cap = cv2.VideoCapture()
        cap.open('videos/qtall.mp4')

        while not cap.isOpened():
            pass

        frame_index = 0
        while cap.isOpened():
            success, frame = cap.read()

            for i in range(10):
                success, frame = cap.read()
                frame_index += 1

                if not success:
                    break

            if not success:
                break

            rgbFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            # rgbFrameResized = rgbFrame
            rgbFrameResized = imutils.resize(rgbFrame, width=720)
            resizeFactor = frame.shape[1] / float(rgbFrameResized.shape[1])
            # boxes = face_recognition.face_locations(rgbFrameResized, model="cnn")
            boxes = face_recognition.face_locations(rgbFrameResized)
            faces = face_recognition.face_encodings(rgbFrameResized, boxes)

            detected_faces = []

            for (index, face) in enumerate(faces):
                matches = face_recognition.compare_faces(
                    [face_item for (face_index, face_name, face_item) in datasetarr], face)
                name = "Unknown"

                if True in matches:

                    matchedIdxs = [i for (i, b) in enumerate(matches) if b]
                    counts = {}

                    for i in matchedIdxs:
                        (face_index, face_name, face_item) = datasetarr[i]
                        counts[face_name] = counts.get(face_name, 0) + 1

                    name = max(counts, key=counts.get)

                detected_faces.append((name, boxes[index]))

            for (name, box) in detected_faces:
                (top, right, bottom, left) = box
                # top = int(top * resizeFactor)
                # right = int(right * resizeFactor)
                # bottom = int(bottom * resizeFactor)
                # left = int(left * resizeFactor)

                cv2.rectangle(
                    rgbFrameResized,
                    (left, top), (right, bottom),
                    (0, 255, 0),
                    2
                )
                y = top - 15 if top - 15 > 15 else top + 15
                cv2.putText(
                    rgbFrameResized,
                    name,
                    (left, y),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    0.75,
                    (0, 255, 0),
                    2
                )

            cv2.putText(
                rgbFrameResized,
                "faces: " + str(len(detected_faces)),
                (0, int(frame.shape[0] * 0.9)),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.75,
                (0, 255, 0),
                2
            )

            cv2.imshow("frame", rgbFrameResized)
            cv2.setWindowTitle("frame", "frame: " + str(frame_index))

            key = cv2.waitKey(1) & 0xFF
            if key == ord("q"):
                break

            frame_index += 1
